// globals
const display = document.getElementById("flex-calc-display");
if (!display) { console.error("Couldn't find the 'display' element!") }

const buttonsContainer = document.querySelector(".flex-btns");
setEventListeners(buttonsContainer)


function setEventListeners(buttons: Element | null) {
    if (buttons) {
        let tokens: string[] = []; // represents the tokens in the calculation
        buttons.querySelectorAll("input").forEach(button => {
            button.addEventListener("click", (event: MouseEvent) => handleButtonClick(event, tokens));
            
        });
    }
    else {
        console.error("Couldn't set event listeners because 'buttons' is null");
    }
}

function isInputElement(elem: EventTarget | null): elem is HTMLInputElement {
    return elem instanceof HTMLInputElement;
}

/*
    format:
    tokens[0] = num1
    tokens[1] = op
    tokens[2] = num2
*/
function updateDisplay(tokens: string[]) {
    if (display)
        display.innerHTML = tokens[0];
    console.log(tokens);
}

function handleButtonClick(event: MouseEvent, tokens: string[]) {
    const clickedButton = event.target;
    if (isInputElement(clickedButton)) {
        /*
           If the button is a number or op, just show it on the display and push it to an array of tokens. (check if the class is 'keypad-btns' or 'op-btns')
           else if the button is the '=' button (check by id), call calc(arrayOfTokens)
           else if the button is a sign (%, ., ..), call signButtonsHandler(arrayOfTokens)
           else: handle the CE or C or DELETE buttons (removeButtonsHandler(arrayOfTokens))
        */
    
        if (clickedButton.className === "keypad-btns" || clickedButton.className === "op-btns") {
            // TODO: multiply the number by 10 and add more digits as the needed
            // 9, NOW PRESSED 5 - do 9*10+5=95; NOW PRESSED 2 - do 95*10+2=950+2=952
            
            if (clickedButton.className === "keypad-btns" && tokens.length > 0) {
                let num1New = parseInt(clickedButton.value, 10);
                tokens[0] = ((parseInt(tokens[0], 10) * 10) + num1New).toString(10);
            }
            else {
                tokens.push(clickedButton.value);
            }

            updateDisplay(tokens);
        }
    }
    else {
        console.error(`(${clickedButton}) is not an HTMLInputElement!`);
    }

}

